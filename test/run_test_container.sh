#!/usr/bin/env bash

readonly SCRIPT_NAME="$0"
readonly SCRIPT_DIR=$(cd "$(dirname $SCRIPT_NAME)"; pwd)

main() {
	src_dir=$(cd $SCRIPT_DIR; cd ..; cd src; pwd)
	echo $src_dir
	docker run -it --rm --network=squid_network -v=$SCRIPT_DIR:/usr/test -v=$src_dir:/usr/src ubuntu /bin/bash -c "echo 'Acquire::http { Proxy \"http://test_squid_proxy:8000\"; };' >> /etc/apt/apt.conf.d/01proxy & /bin/bash /usr/src/main_setup.sh"
}

main