#!/usr/bin/env bash

readonly STUDIO_URL=https://dl.google.com/dl/android/studio/ide-zips/3.0.1.0/android-studio-ide-171.4443003-linux.zip

form_launcher() {
	local name=$1
	local icon_path=$2
	local exec_path=$3
	local wm_class=$4
	local file_path=$5
   	local content="[Desktop Entry]
Version=1.0
Type=Application
Name=${name}
Icon=${icon_path}
Exec=\"${exec_path}\" %f
Comment=The Drive to Develop
Categories=Development;IDE;
Terminal=false
StartupWMClass=${wm_class}"
	echo "${content}" > "${file_path}"
}

install_libs_for_android_studio() {
	apt-get install -y libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386 2>&1
}

download_android_studio() {
	local downloads_dir=$1
	local zip_url=$2
	(cd $downloads_dir; wget $zip_url)
    #verify download finished
    if [ ! -f ${downloads_dir}/android-studio* ]; then
        echo "zip not found. Download failed?"
        exit 1
    fi
}

place_android_studio() {
	local downloads_dir=$1
	local install_dir=$2
	local studio_zip=$(ls ${downloads_dir} | grep android-studio)
    unzip ${downloads_dir}/$studio_zip -d ${install_dir}
    chmod 755 -R ${install_dir}/android-studio/
}

create_android_studio_lancher() {
	local install_dir=$1
	local app_name="Android Studio"
	local app_icon_path="${INSTALL_DIR}/android-studio/bin/studio.png"
	local app_exec_path="${INSTALL_DIR}/android-studio/bin/studio.sh"
	local app_wm_class="jetbrains-studio"
	local app_launcher_dir_path="/usr/share/applications"
	if [ ! -d $app_launcher_dir_path ]; then
		mkdir -p $app_launcher_dir_path
	fi

	local app_launcher_path="${app_launcher_dir_path}/jetbrains-studio.desktop"
	if [ -e "${app_launcher_path}" ]; then
    	echo "A launcher already exists."
	else 
		$( form_launcher "${app_name}" "${app_icon_path}" "${app_exec_path}" "${app_wm_class}" "${app_launcher_path}")
	    echo "A launcher created."
	fi 
}

install_android_studio() {
	if [ ! -f $DOWNLOADS/android-studio* ]; then
	    download_android_studio $DOWNLOADS $STUDIO_URL
	fi

	if [ ! -d $INSTALL_DIR/android-studio ]; then
	    place_android_studio $DOWNLOADS $INSTALL_DIR
	    create_android_studio_lancher $INSTALL_DIR
	fi
}

main() {
	readonly DOWNLOADS="${LS_DOWNLOAD_DIR}"
	readonly INSTALL_DIR="${LS_INSTALL_DIR}"
	if [ ! -d $DOWNLOADS ]; then
		mkdir -p $DOWNLOADS
	fi
	install_libs_for_android_studio
	install_android_studio
}

main