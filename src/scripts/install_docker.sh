#!/usr/bin/env bash

main() {
	echo "Installing Docker"
 	apt --yes install docker.io
	echo "Installing Docker Compose"
	apt --yes install docker-compose

	echo "Setup Docker"
	# gives access to docker without sudo. requires restart after
	user=$(whoami)
	usermod -a -G docker $user
}

main