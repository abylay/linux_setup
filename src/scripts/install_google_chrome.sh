#!/usr/bin/env bash

main() {
	# Chrome:key|repo
	wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
	sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

	# Update
	apt-get update 

	echo "Installing Google Chrome"
	apt-get -y install libindicator7
	apt-get -f -y install
	apt-get -y install google-chrome-stable
}

main