#!/usr/bin/env bash

readonly SCRIPT_NAME="$0"
readonly SCRIPT_DIR=$(cd "$(dirname $SCRIPT_NAME)"; pwd)

main() {
	docker run -d -p 8000:8000 --network=squid_network -v=$SCRIPT_DIR/tmp/cache:/var/cache/squid-deb-proxy --name test_squid_proxy im_squid_proxy

	docker cp $SCRIPT_DIR/enable.conf test_squid_proxy:/etc/squid-deb-proxy/allowed-networks-src.acl.d/enable.conf

	docker cp $SCRIPT_DIR/no_cache.conf test_squid_proxy:/etc/squid-deb-proxy/mirror-dstdomain.acl.d/no_cache.conf

	docker restart test_squid_proxy
}

main