#!/usr/bin/env bash

#************************************************#
#               main_setup.sh                    #
#           written by Abylay Sabirgaliyev       #
#                February 17, 2018               #
#                                                #
#           Setup development environment.       #
#************************************************#

# Use set -o xtrace (a.k.a set -x) to trace what gets executed. Useful for debugging.
# set -o xtrace
# Use set -o errexit (a.k.a. set -e) to make your script exit when a command fails.
set -o errexit
# Use set -o nounset (a.k.a. set -u) to exit when your script tries to use undeclared variables.
set -o nounset
# Use set -o pipefail in scripts to catch mysqldump fails in e.g. mysqldump |gzip. The exit status of the last command that threw a non-zero exit code is returned.
# set -o pipefail

set -euox pipefail

# Immutable global variables
readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"


setup_config() {
	source $PROGDIR/main.conf
	echo "${LS_DOWNLOAD_DIR}"
	echo "${LS_INSTALL_DIR}"
}

make_scrpts_executable() {
	for f in $PROGDIR/scripts/*.sh; do 
		chmod +x $f; 
	done
}

install_utils() {
	if ! [ -x "$(command -v wget)" ]; then
	  apt-get install wget --assume-yes
	fi

	if ! [ -x "$(command -v curl)" ]; then
	  apt-get install curl --assume-yes
	fi

	if ! [ -x "$(command -v unzip)" ]; then
	  apt-get install unzip --assume-yes
	fi

	#libs
	apt-get install software-properties-common --assume-yes
	apt-get install apt-transport-https --assume-yes
}

add_arch() {
	if ! dpkg --print-foreign-architectures | grep -q 'i386'; then
	  	dpkg --add-architecture i386
		apt-get update
	fi
}

# does everything
main() {
	echo start everything
	make_scrpts_executable
	setup_config
	apt-get update
	add_arch
	install_utils
	# source $PROGDIR/scripts/install_android_studio.sh
	# source $PROGDIR/scripts/install_java_8.sh
	# source $PROGDIR/scripts/install_google_chrome.sh
	# source $PROGDIR/scripts/install_sublime_3.sh
	# source $PROGDIR/scripts/install_git.sh
	# source $PROGDIR/scripts/setup_gitlab_keys.sh
	# source $PROGDIR/scripts/install_docker.sh
	source $PROGDIR/scripts/install_gitkraken.sh
	echo end everything
}

main