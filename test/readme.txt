
docker build -t im_squid_proxy -f test2/Dockerfile .

docker network create squid_network

docker run -d -p 8000:8000 --network=squid_network -v=$(pwd)/tmp/cache:/var/cache/squid-deb-proxy --name test_squid_proxy im_squid_proxy

docker cp ./test2/test.conf test_squid_proxy:/etc/squid-deb-proxy/allowed-networks-src.acl.d/test.conf

docker run -it --rm --network=squid_network -v=$(pwd):/usr/test ubuntu /bin/bash

echo 'Acquire::http { Proxy "http://test_squid_proxy:8000"; };' >> /etc/apt/apt.conf.d/01proxy



---

docker exec -it test_squid_proxy bash

add additional source hosts as expalined here:
https://askubuntu.com/questions/119298/apt-get-using-apt-cacher-ng-fails-to-fetch-packages-with-hash-sum-mismatch

docker restart test_squid_proxy
