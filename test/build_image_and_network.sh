#!/usr/bin/env bash

readonly SCRIPT_NAME="$0"
readonly SCRIPT_DIR=$(cd "$(dirname $SCRIPT_NAME)"; pwd)

main() {
	docker build -t im_squid_proxy -f $SCRIPT_DIR/Dockerfile $SCRIPT_DIR

	docker network create squid_network
}

main