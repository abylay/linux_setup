#!/usr/bin/env bash

main() {
	docker network rm squid_network

	docker rmi im_squid_proxy
}

main