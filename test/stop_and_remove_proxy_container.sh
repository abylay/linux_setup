#!/usr/bin/env bash

main() {
	docker stop test_squid_proxy

	docker rm test_squid_proxy
}

main