#!/usr/bin/env bash

main() {
	local downloads_dir="${LS_DOWNLOAD_DIR}"
	local deb_url='https://release.gitkraken.com/linux/gitkraken-amd64.deb'
	wget $deb_url --directory-prefix=$downloads_dir
	dpkg -i ${downloads_dir}/gitkraken-amd64.deb
}

main