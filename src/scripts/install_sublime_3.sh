#!/usr/bin/env bash

main() {
	# Sublime Text 3:key|repo
	wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -
	echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list

	# Update
	apt-get update 

	echo "Installing Sublime Text 3"
	apt-get -y install sublime-text
}

main