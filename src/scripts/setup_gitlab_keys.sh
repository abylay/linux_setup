#!/usr/bin/env bash

main() {
	# gitlab token
	token="${LS_GITLAB_TOKEN}"
	echo $token

	keydir="${HOME}/.ssh"
	if [ ! -d $keydir ]; then
		mkdir -p $keydir
	fi
	# generate ssh key pair if there are not any
	ssh-keygen -q -f $keydir/id_rsa -t rsa -N ''

	chmod -R 755 $keydir

	# public key
	key_pub=$(<~/.ssh/id_rsa.pub)
	echo "$key_pub"
	
	# key title
	title="$(hostname) $(date)"

	# upload public key
	curl --data-urlencode "key=${key_pub}" --data-urlencode "title=${title}" \
		https://gitlab.com/api/v3/user/keys?private_token="$token"
}

main