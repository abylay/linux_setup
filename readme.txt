
Development

Most bash scripts can be tested in docker containers.

Running scripts in a docker container:

1. Run container with a mounted volume 
	docker run -it --rm -v=$(pwd):/usr/test ubuntu /bin/bash
It should create, start a container and connect to bash terminal.
Container will be deleted on exit.

2. Go to mounted dir:
	cd /usr/test

3. Run the main script:
	./main_setup.sh


Running scripts in a docker container with package cache:

1. 
	
./test/build_image_and_network.sh 

sudo ./test/prepare_dirs.sh 

./test/run_proxy_container.sh

