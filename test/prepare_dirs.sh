#!/usr/bin/env bash
# run this as root
readonly SCRIPT_NAME="$0"
readonly SCRIPT_DIR=$(cd "$(dirname $SCRIPT_NAME)"; pwd)

main() {
	chmod -R 777 $SCRIPT_DIR/tmp
}

main