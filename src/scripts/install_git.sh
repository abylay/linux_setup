#!/usr/bin/env bash

main() {
	local username="${LS_GIT_USER}"
	local user_email="${LS_GIT_EMAIL}"

	echo "Installing Git"
	apt-get -y install git-core

	echo "Setup Git"
	git config --global user.name "${username}"
	git config --global user.email "${user_email}"
}

main