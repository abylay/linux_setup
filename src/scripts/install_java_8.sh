#!/usr/bin/env bash

main() {
	# Java8:repo
	add-apt-repository ppa:webupd8team/java -y
	apt-get update 
	echo "Installing Java8"
	echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections 
	apt-get install oracle-java8-installer --assume-yes
	echo "Settings Java variables"
	apt-get install oracle-java8-set-default --assume-yes
}

main